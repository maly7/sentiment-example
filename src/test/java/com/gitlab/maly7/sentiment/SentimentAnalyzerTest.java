package com.gitlab.maly7.sentiment;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SentimentAnalyzerTest {
    private SentimentAnalyzer sentimentAnalyzer;

    @Before
    public void setup() {
        sentimentAnalyzer = new SentimentAnalyzer();
    }

    @Test
    public void veryPositiveSentiment() {
        assertEquals("Very positive", sentimentAnalyzer.calculateSentiment("Sentiment analysis is fun!"));
    }

    @Test
    public void postitiveSentiment() {
        assertEquals("Positive", sentimentAnalyzer.calculateSentiment("Pizza tastes good."));
    }

    @Test
    public void neutralSentiment() {
        assertEquals("Neutral", sentimentAnalyzer.calculateSentiment("Pizza is."));
    }

    @Test
    public void negativeSentiment() {
        assertEquals("Negative", sentimentAnalyzer.calculateSentiment("Pizza doesn't taste very good."));
    }
}
