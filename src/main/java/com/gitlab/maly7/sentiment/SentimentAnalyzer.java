package com.gitlab.maly7.sentiment;

import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.sentiment.SentimentCoreAnnotations;
import edu.stanford.nlp.util.CoreMap;

import java.util.List;
import java.util.Properties;

public class SentimentAnalyzer {

    private static StanfordCoreNLP pipeline = buildPipeline();

    public String calculateSentiment(String text) {
        Annotation document = new Annotation(text);
        pipeline.annotate(document);

        List<CoreMap> annotations = document.get(CoreAnnotations.SentencesAnnotation.class);
        return annotations.get(0).get(SentimentCoreAnnotations.SentimentClass.class);
    }

    private static StanfordCoreNLP buildPipeline() {
        Properties props = new Properties();
        props.setProperty("annotators", "tokenize,ssplit,pos,parse,sentiment");
        props.setProperty("language", "english");
        props.setProperty("ssplit.isOneSentence", "true");
        props.setProperty("tokenize.class", "PTBTokenizer");
        props.setProperty("tokenize.language", "en");
        return new StanfordCoreNLP(props);
    }

}
