package com.gitlab.maly7;

import com.gitlab.maly7.sentiment.SentimentAnalyzer;

public class Application {
    private static final SentimentAnalyzer SENTIMENT_ANALYZER = new SentimentAnalyzer();

    public static void  main(String... args) {
        System.out.println(SENTIMENT_ANALYZER.calculateSentiment("Sentiment is fun!"));

        System.out.println(SENTIMENT_ANALYZER.calculateSentiment("I hate tests!"));
    }
}
